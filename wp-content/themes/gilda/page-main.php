<?php
/**
 * Template Name: Top Level Navigation
 */

get_header(); ?>


	
<div class="wrap interior-wrap">
	
		<h1 class="entry-title"><?php the_field('title'); ?></h1>

<!-- 		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?> -->
	
	
		<div class="featured">
			<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
		</div>
		
		
		<?php if( get_field('intro_text') ): ?>
			<div class="intro-text"><?php the_field('intro_text'); ?></div>
		<?php endif; ?>
		
		<div class="content-circles">
		<?php

		// check if the repeater field has rows of data
		if( have_rows('content_circles') ):?>
		
		 	<?php // loop through the rows of data
		    while ( have_rows('content_circles') ) : the_row();?>
		
		       <h6><span><?php // display a sub field value
		        the_sub_field('label');?></span></h6>
		
		    <?php endwhile;
		
		else :
		
		    // no rows found ?>
		
		<?php endif;
		
		?>
	</div>
	<div id="accordion">
	<?php

		// check if the repeater field has rows of data
		if( have_rows('accordion') ):?>
	
		
		<?php // loop through the rows of data
		    while ( have_rows('accordion') ) : the_row();?>
			<!--accordion-->
			<div class="tab-container">
			  <div class="tab" role="button">
			    <?php // display a sub field value
		        the_sub_field('title');?><div class="tab-arrow"><i class="fas fa-sort-down"></i></div>
			  </div>
			  <div class="slide">
			    <?php // display a sub field value
		        the_sub_field('content');?>
			  </div>
			</div>
			<!--/accordion-->
			

		
		<?php endwhile;
		
		else :
		
		    // no rows found ?>
		
		<?php endif;
		
		?>
		</div>	
				<?php
				// Show the selected front page content.
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/page/content', 'page' );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif;
				?>
			  

</div>
	
	
	




<?php
get_footer();
