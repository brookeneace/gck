<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		
			<section id="hero-cards">
				<div class="wrap">
					
					<div class="card">
						<div class="photo"><img src="<?php bloginfo('template_url')?>/assets/images/photo-join.png" alt="Join"/></div>
						<div class="card-content">
							<h1>Join</h1>
							<p><?php the_field('join_text'); ?></p>
							
							<a href="<?php bloginfo('url') ?>/join"><button class="member">Become a member</button></a>
						</div>
					</div>
					
					<div class="card">
						<div class="photo"><img src="<?php bloginfo('template_url')?>/assets/images/photo-act.png" alt="Act"/></div>
						<div class="card-content">
							<h1>Act</h1>
							<p><?php the_field('act_text'); ?></p>
							
							<a href="<?php bloginfo('url') ?>/volunteer"><button class="vol">Volunteer</button></a>
						</div>
					</div>
					
					<div class="card">
						<div class="photo"><img src="<?php bloginfo('template_url')?>/assets/images/photo-give.png" alt="Give"/></div>
						<div class="card-content">
							<h1>Give</h1>
							<p><?php the_field('give_text'); ?></p>
							
							<a href="https://gcl.gnosishosting.net/Portal/Donate"><button class="donate">Make a donation</button></a>
						</div>
					</div>
					
				</div>
			</section>
		
				<?php
				// Show the selected front page content.
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/page/content', 'front-page' );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif;
				?>
				
		<section id="special-events" class="parallax" data-image-src="<?php bloginfo('template_url')?>/assets/images/bg-special-events.png">
			
			<div class="wrap events-wrap">
				
				<div class="events-title">Special Events</div>
			
				<div class="events-list">
					<img src="<?php bloginfo('template_url')?>/assets/images/label-comming-soon.png" alt="Coming Soon" class="label"/>
								<?php
						
						        // The Arguments
						        $args = array(
						            'post_type' => 'events', 
						            'posts_per_page'         => '3',
						            
						        );  
						
						        // The Query
						        $the_query = new WP_Query( $args ); ?>
						
						        <?php
						
						        // If we have the posts...
						        if ( $the_query->have_posts() ) : ?>
						
						        <!-- Start the loop the loop --> 
						            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
										<div class="single-event-card">
						                <a href="<?php the_permalink(); ?>">
							                <span class="date"><?php the_field('date'); ?></span>
							                <h5><?php the_title(); ?></h5>
							            </a>
										</div>
						            <?php endwhile; endif; // end of the loop. ?>
						
						        <?php wp_reset_postdata(); ?>
						        
						<div class="link-all-events">
							<a href="events/">All Special Events</a> | <a href="https://gcl.gnosishosting.net/Events/Calendar">View Program Calendar</a>
						</div>
				</div>
			</div>
			
		</section>
		
		<section id="sponsors">
			<div class="wrap">
				<h2>Our Sponsors</h2>
				<div class="sponsor-logos">
					
					
				<?php

				// check if the repeater field has rows of data
				if( have_rows('sponsor_logos') ):?>
				
				 	<?php // loop through the rows of data
				    while ( have_rows('sponsor_logos') ) : the_row();?>
				
				       <div><img src="<?php the_sub_field('logo');?>" alt="Sponsor Logo"/></div>
				
				    <?php endwhile;
				
				else :
				
				    // no rows found ?>
				
				<?php endif;
				
				?>
<!--
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-humana.png" alt="Humana"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-amazon.png" alt="Amazon"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-jgb.png" alt="James Grahm Brown Cancer Center"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-kelly-construction.png" alt="Kelley Construction"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-kroger.png" alt="Kroger"/></div>
				  
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-humana.png" alt="Humana"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-amazon.png" alt="Amazon"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-jgb.png" alt="James Grahm Brown Cancer Center"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-kelly-construction.png" alt="Kelley Construction"/></div>
				  <div><img src="<?php bloginfo('template_url')?>/assets/images/logo-kroger.png" alt="Kroger"/></div>
-->
				
				</div>
		
			</div>
		</section>
		  
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
