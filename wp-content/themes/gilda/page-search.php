<?php
/**
 * Template Name: Search
 */

get_header(); ?>


	
<div class="wrap interior-wrap">
	
	<h1 class="entry-title"><?php the_title(); ?></h1>
	
	
				<?php
				// Show the selected front page content.
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/page/content', 'page' );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif;
				?>
				
				<?php get_search_form(); ?>
			  

</div>
	
	
	




<?php
get_footer();
