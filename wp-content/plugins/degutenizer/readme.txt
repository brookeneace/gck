=== DeGutenizer ===
Contributors: pipdig
Tags: gutenburg, disable gutenberg, editor, classic editor
Requires at least: 4.9
Tested up to: 4.9
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

De-gutenize your site.

== Description ==

This plugin is no longer required. Please use the official [Classic Editor plugin](https://wordpress.org/plugins/classic-editor/) instead.

== Changelog ==

= 1.1 =
* This plugin is no longer required. Please use the official [Classic Editor plugin](https://wordpress.org/plugins/classic-editor/) instead.

= 1.0 =
Initial release!