<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,700,700i,900" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	





	<div class="site-content-contain">
		<div id="content" class="site-content">
			
			
				<header>
					<div class="wrap">
						<a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url')?>/assets/images/gilda-logo.png" alt="Gilda's Club of Kentuckiana" /></a>
						<?php wp_nav_menu( array(
				    'menu'           => 'Main'
					) );?>
						<div class="mobile-nav-toggle">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="header-search"><a href="<?php bloginfo('url') ?>/search"><i class="fas fa-search"></i></a></div>
					</div>

				</header>
			
				