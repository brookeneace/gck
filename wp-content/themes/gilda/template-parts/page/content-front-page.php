<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>"  >



		<div class="wrap">
			
				
		</div><!-- .wrap -->

			<div class="entry-content home-motto">
				<div class="wrap wrap-motto">
					<img src="<?php bloginfo('template_url')?>/assets/images/label-motto.png" alt="Our Motto" class="label"/>
					<?php
					/* translators: %s: Name of current post */
					the_content();
					?>
				</div>
			</div><!-- .entry-content -->

		


</article><!-- #post-## -->
