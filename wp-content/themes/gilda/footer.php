<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
		
		<a href="https://gcl.gnosishosting.net/Portal/Donate"><button class="donate-fab">Donate Now</button></a>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
			<div class="footer-left">
			
					<aside>
						<a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url')?>/assets/images/gilda-footer-logo.png" alt="Gilda's Club of Kentuckiana" /></a><br/>
						<p>&copy; <?php echo date("Y"); ?> Gilda's Club Kentuckiana</p>
						<p><a href="<?php bloginfo('url')?>/sitemap">Sitemap</a> | <a href="<?php bloginfo('url')?>/privacy-policy">Privacy Policy</a></p>
						
					</aside>
					<aside>
					<?php wp_nav_menu( array(
				    'menu'           => 'Footer'
					) );?>
					</aside>
			
			</div>
				<div class="footer-right">
					<h3>Find Us</h3>
					<p><?php the_field('address', 'option'); ?></p>
					<a href="https://www.facebook.com/GildasClubKY/" target="_blank"><i class="fab fa-facebook-f"></i></a>
					<a href="https://twitter.com/GildasClubLou" target="_blank"><i class="fab fa-twitter"></i></a>
					<a href="https://www.instagram.com/gildasclubkentuckiana/" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="https://www.youtube.com/user/gildasclublouisville" target="_blank"><i class="fab fa-youtube"></i></a>
					
					<img src="<?php bloginfo('template_url')?>/assets/images/footer-logo-charitynav.png" alt="Charity Navigator" />
					<img src="<?php bloginfo('template_url')?>/assets/images/footer-logo-bbb.png" alt="Better Business Bureau" />
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
