<?php
/**
 * Template Name: Site Map
 */

get_header(); ?>


	
<div class="wrap interior-wrap">
	
		<h1 class="entry-title"><?php the_title(); ?></h1>


	
	
		<div class="featured">
			<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
		</div>
		
		
			

		
			
				<?php
				// Show the selected front page content.
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/page/content', 'page' );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif;
				?>
			  <?php wp_nav_menu( array(
				    'menu'           => 'Sitemap'
					) );?>

</div>
	
	
	




<?php
get_footer();
